#!/usr/bin/python3

# Generate a LibreLingo course skeleton from a YAML

# TODO: add translations for the code printing (e.g. gettext)

import sys
import os
import argparse

import argostranslate.package, argostranslate.translate
from yaml import safe_load
from yaml import dump as dump_yaml

# sys.path.append("librelingo_types")

#from librelingo_types import (
#    AudioSettings,
#    Course,
#    DictionaryItem,
#    Language,
#    License,
#    Module,
#    Phrase,
#    Settings,
#    Skill,
#    TextToSpeechSettings,
#    Word,
#)

id_counter: int = 1
# Default source and target language
from_lang: str = "en"
to_lang: str = "es"
reverse: bool = False


def _load_yaml(path):
    """Helper function for reading a YAML file"""
    with open(path) as yaml_file:
        return safe_load(yaml_file)


def install_languages(source_language: str = "en", target_language: str = "es"):
    """Install languages to translate"""
    available_packages = argostranslate.package.get_available_packages()
    language_pair = list(
        filter( lambda x: x.from_code == source_language and x.to_code == target_language, available_packages)
    )[0]
    print(f"Installing languages {source_language}, {target_language}", file=sys.stderr)
    download_path = language_pair.download()
    argostranslate.package.install_from_path(download_path)


def translate(phrase: str, source_language: str = "en", target_language: str = "es"):
    """
    Translate a phrase from one language to another (with their codes e.g. "en", "es")
    """

    installed_languages = argostranslate.translate.get_installed_languages()

    def get_languages(source_language, target_language):
        from_lang = list(filter(
            lambda x: x.code == source_language,
            installed_languages))[0]

        to_lang = list(filter(
            lambda x: x.code == target_language,
            installed_languages))[0]

        return from_lang, to_lang

    # Install languages if not previously installed
    try:
        from_lang, to_lang = get_languages(source_language, target_language)
    except:
        install_languages(source_language, target_language)
        from_lang, to_lang = get_languages(source_language, target_language)

    # Translate
    translation = from_lang.get_translation(to_lang)
    return translation.translate(phrase)


def generate_skill(
        skilldict: dict,
        skillname: str,
        skills_dir: str = os.getcwd(),
        # reverse: bool = False
    ) -> None:
    """
    Generate a YAML file for a LibreLingo skill from a dictionary
    """
    # TODO:
    # - pair the skilldict with the Skill type in the LL repo
    # - replace the globals for decorators to this function

    global id_counter
    global from_lang, to_lang
    global reverse

    print(f"  Generating skill '{skillname}'", file=sys.stderr)

    for _ in 'New words', 'Phrases':
        if not _ in skilldict:
            skilldict[_] = []

    punctuations = ',;.!?"\(\)\[\]\{\}=&¿¡'

    print("    Translating words and phrases", file=sys.stderr)

    # Translated Phrases and New Words
    phrases_tr = [ translate(phrase, from_lang, to_lang)
                    for phrase in skilldict['Phrases'] ]
    newwords_tr = [ translate(word, from_lang, to_lang)
                    for word in skilldict['New words'] ]

    # TODO: more elegant way of reversing the translation
    if reverse:
        phrases_tr, skilldict['Phrases'] = skilldict['Phrases'], phrases_tr
        newwords_tr, skilldict['New words'] = skilldict['New words'], newwords_tr
        from_lang, to_lang = to_lang, from_lang

    from re import sub as re_sub

    # Only words separated by spaces, for the mini-dictionary
    words = ' '.join(skilldict['Phrases'] + skilldict['New words'])
    words = re_sub(f'[{punctuations}]', '', words).lower()
    words = set(words.split())

    # Only translated words separated by spaces, for the mini-dictionary
    words_tr = ' '.join(phrases_tr + newwords_tr)
    words_tr = re_sub(f'[{punctuations}]', '', words_tr).lower()
    words_tr = set(words_tr.split())

    # Transform the arrays of 'new words' and 'phrases'
    # to the correct format for skills

    skilldict['New words'] = [{
        "Word": word,
        "Translation": word_tr
        } for word, word_tr in zip(skilldict['New words'], newwords_tr) ]

    skilldict['Phrases'] = [{
        "Phrase": word,
        "Translation": word_tr
        } for word, word_tr in zip(skilldict['Phrases'], phrases_tr) ]

    skilldict['Skill'] = {
            'Name': skillname,
            'Id': id_counter
            }

    id_counter += 1

    print("    Generating mini-dictionary", file=sys.stderr)

    # TODO: put the language name instead of its IETF BCP 47 code
    skilldict['Mini-dictionary'] = {
            from_lang: [ { word: [translate(word, from_lang, to_lang)] }
                        for word in words],
            to_lang: [ { word: [translate(word, to_lang, from_lang)] }
                        for word in words_tr ]
            }

    with open(os.path.join(skills_dir, skillname + '.yaml').lower(), 'w') as fh:
        dump_yaml(skilldict, fh, allow_unicode=True)


def generate_module(
        moduledict: dict,
        modulename: str,
        course_dir: str = os.getcwd(),
    ) -> None:
    """
    Generate directory tree for a LibreLingo module from a dictionary
    """

    print(f"Generating module '{modulename}'", file=sys.stderr)

    module_dir = os.path.join(course_dir, modulename).lower()
    skills_dir = os.path.join(module_dir, 'skills').lower()

    os.mkdir(module_dir)
    os.mkdir(skills_dir)

    # skills = [ _.lower() for _ in moduledict.keys() ]
    skills = moduledict.keys()

    module_yaml: dict  = {
            'Module': {
                'Name': modulename  # Double quotes around it get single quoted automatically when writing
                },
            'Skills': [skill.lower() + '.yaml' for skill in skills]
            }

    print(f"Writing module.yaml", file=sys.stderr)
    with open(os.path.join(module_dir, 'module.yaml'), 'w') as fh:
        dump_yaml(module_yaml, fh, allow_unicode=True)

    for skill in skills:
        generate_skill(moduledict[skill], skill, skills_dir)


def generate_course(
        coursedict: dict,
        root_dir = os.getcwd()
    ) -> None:
    """
    Generate directory tree for a LibreLingo course from a dictionary
    """

    course_dir = 'course'
    os.mkdir(os.path.join(root_dir, course_dir))

    modules = coursedict.keys()

    course_yaml: dict = {
            "Modules" : [module.lower() + '/' for module in modules]
            }

    print(f"Writing course.yaml", file=sys.stderr)
    with open(os.path.join(course_dir, 'course.yaml'), 'w') as fh:
        dump_yaml(course_yaml, fh, allow_unicode=True)

    for module in modules:
        generate_module(coursedict[module], module, course_dir)


def parse_cmdargs(cmdargs: list = []):
    """Command-line argument parsing"""
    # TODO:
    # - option to specify name of output file/root directory
    # - option for skill subparser to print to stdin instead of generating file
    # - option for quiet/verbose, and make quiet the default
    # - option to identify the top key of the yaml as the name of the course/module/skill

    parser = argparse.ArgumentParser(
            description = '''
            Tool to easily generate LibreLingo courses, modules and skills, with translations
            ''')

    parser.add_argument('-r', '--reverse',
            help = "Reverse which is the source and target language",
            default = False,
            action = 'store_true'
            )

    parser.add_argument('-f', '--from-lang',
            help = "Source language to translate from, IETF BCP 47 tag name",
            default = "en"
            )
    parser.add_argument('-t', '--to-lang',
            help = "Target language to translate to, IETF BCP 47 tag name",
            default = "es"
            )

    subparsers = parser.add_subparsers(help='Subcommand to use', dest='subcmd')

    parser_course = subparsers.add_parser('course')
    parser_course.add_argument('file')

    parser_module = subparsers.add_parser('module')
    parser_module.add_argument('file')

    parser_skill = subparsers.add_parser('skill')
    parser_skill.add_argument('file')

    return parser.parse_args(cmdargs)


def main():

    args = parse_cmdargs(sys.argv[1:])

    if 'from_lang' in args:
        globals()['from_lang'] = args.from_lang

    if 'to_lang' in args:
        globals()['to_lang'] = args.to_lang

    if 'reverse' in args:
        globals()['reverse'] = args.reverse

    # TODO: accept yaml files from stdin
    # ↓ make course subcommand the default
    if args.subcmd == 'course':
        coursedict: dict = _load_yaml(args.file)
        generate_course(coursedict)
    elif args.subcmd == 'module':
        moduledict: dict = _load_yaml(args.file)
        modulename: str = os.path.splitext(args.file)[0]
        generate_module(moduledict, modulename)
    elif args.subcmd == 'skill':
        skilldict: dict = _load_yaml(args.file)
        skillname: str = os.path.splitext(args.file)[0]
        generate_skill(skilldict, skillname)
        # Note: this would replace the original yaml skill file


if __name__ == "__main__":
    main()
