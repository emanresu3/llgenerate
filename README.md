# LibreLingo generate

A script to bootstrap a LibreLingo course, module or skill from a simple YAML, with automatic translations.

## Dependencies

- Python3
- Argos-translate
- PyYAML

## Usage

You can use this to easily bootstrap courses, modules or skills

### Courses
Write a YAML file with this format:

```yaml
# coursefile.yaml
module_name1:
 skill_name1:
   New words:
     - word1
     - word2
   Phrases:
     - phrase1
     - phrase2
 skill_name2:
   Phrases:
     - phrase1
module_name2:
 skill_name1:
   Phrases:
     - phrase1
     - phrase2
 skill_name2:
   New words:
     - word1
   Phrases:
     - phrase1
     - phrase2
```

Run:
```bash
python3 llgenerate.py course 'coursefile.yaml'
```

Result:
```
course
├── module_name1
│   ├── skills
│   │   ├── skill_name1.yaml
│   │   └── skill_name2.yaml
│   └── module.yaml
├── module_name2.yaml
│   ├── skills
│   │   ├── skill_name1.yaml
│   │   └── skill_name2.yaml
│   └── module.yaml
└── course.yaml
```

By default it makes translations from English to Spanish, but you can specify the source and target language, for example:

```bash
python3 llgenerate.py -t it course 'coursefile.yaml'        # From English to Italian
python3 llgenerate.py -f it course 'coursefile.yaml'        # From Italian to Spanish
python3 llgenerate.py -f fr -t it course 'coursefile.yaml'  # From French to Italian
```

If you write the sentences in language A, they would be translated to language B, thus making it a
LibreLingo course A-from-B (so the default is EN-from-ES), which might be confusing. If you want
the course to be B-from-A, just add the `-r` option:

```bash
python3 llgenerate.py course 'coursefile.yaml'                 # Course English from Spanish (sentences in English)
python3 llgenerate.py -r course 'coursefile.yaml'              # Course Spanish from English (sentences in English)
python3 llgenerate.py -r -f fr -t it course 'coursefile.yaml'  # Course French from Italian (sentences in French)
```

### Modules
Write a YAML file with this format:

```yaml
skill_name1:
  New words:
    - word1
    - word2
  Phrases:
    - phrase1
    - phrase2
skill_name2:
  Phrases:
    - phrase1
```

Run:
```bash
python3 llgenerate.py module 'module.yaml'
```

### Skills

Write a YAML file with this format:

```yaml
New words:
  - word1
  - word2
Phrases:
  - phrase1
  - phrase2
```

Run:
```bash
python3 llgenerate.py skill 'skill.yaml'
```
